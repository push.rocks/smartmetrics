import * as plugins from './plugins.js';

import { expect, tap } from '@pushrocks/tapbundle';
import * as smartmetrics from '../ts/index.js';

let testSmartMetrics: smartmetrics.SmartMetrics;

tap.test('should create a smartmetrics instance', async () => {
  const logger = new plugins.smartlog.Smartlog({
    logContext: null,
    minimumLogLevel: 'silly',
  });
  logger.enableConsole();
  testSmartMetrics = new smartmetrics.SmartMetrics(logger, 'testContainer');
  expect(testSmartMetrics).toBeInstanceOf(smartmetrics.SmartMetrics);
});

tap.test('should start smartmetrics', async () => {
  testSmartMetrics.start();
});

tap.test('should produce valid metrics', async (tools) => {
  console.log('calling .getMetrics from Testfile:');
  console.log(await testSmartMetrics.getMetrics());
});

tap.start();
