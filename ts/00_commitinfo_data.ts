/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartmetrics',
  version: '2.0.6',
  description: 'easy system metrics'
}
