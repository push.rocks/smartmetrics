// this might be extracted into a package @pushrocks/smartmetrics-interfaces in the future
export interface IMetricsSnapshot {
  process_cpu_seconds_total: number;
  nodejs_active_handles_total: number;
  nodejs_active_requests_total: number;
  nodejs_heap_size_total_bytes: number;
  cpuPercentage: number;
  cpuUsageText: string;
  memoryPercentage: number;
  memoryUsageBytes: number;
  memoryUsageText: string;
}
